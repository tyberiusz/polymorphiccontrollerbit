module ApplicationHelper

  def commentable_url
    commentable_plu = controller.controller_name
    commentable_sin = commentable_plu.singularize
    commentable_parent_id = "#{commentable_sin}_id".to_sym.downcase
    
    id = controller.instance_variable_get("@#{commentable_sin}").id
    parametres_arr = {commentable_parent_id => id, :commentable_type => commentable_sin, :commentable_id => id}

    case commentable_plu
      when "projects" then project_comments_path(parametres_arr)
      when "users" then user_comments_path(parametres_arr)
    end
  end


end


Rails.application.routes.draw do
  concern :comments do
    resources :comments, path: '/komentarze', shallow: true, as: 'comments'
  end

  resources :projects, concerns: :comments
  resources :users, concerns: :comments

  root "projects#index"
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
